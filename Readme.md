# Social Network
#### REST API for authorization, creating the posts and like or unlike
(Django Rest framework) <br>
#### Structure of application:
- auth rest
    + source
        * main
            - settings.py
            - urls.py
        * users
            - admin.py
            - models.py
                * user model
                * user login activity model
            - serializers.py
            - views.py
            - urls.py
        * webapp
            - admin.py
            - models.py
                * post model
                * post like model
            - serializers.py
            - views.py
            - urls.py
            

#### Basic Features:
- User signup - localhost:8000/api/register > username: input , email: input, password: input 
- User login - localhost:8000/api/login (or by DFR 'Login' button) > username: input, email: input,  password: input.
JWT token is used
- Post creation - localhost:8000/api/posts > in DFR under the list of posts there is creation table (standard DFR ModelViewSet)
, and post is created only by authorized user
- Post like -  localhost:8000/api/posts/id/like > implemented as extra actions
- Post unlike -  localhost:8000/api/posts/id/unlike > implemented as extra actions
- Analytics about how many likes were made in certain range of dates >
localhost:8000/api/analytics/?date_from=Y-M-D&date_to=Y-M-D (or 8000/api/analytics/ and pressing the filter button in DFR)
- User activity endpoint to monitor users logged in - 8000/admin/users/userloginactivity/ > superuser username = admin, password = admin