from django.urls import include, path
from rest_framework import routers
from webapp import views

router = routers.DefaultRouter()
router.register(r'posts', views.PostViewSet)


urlpatterns = [
    path('', include(router.urls)),    # CRUD for posts and extra likes&unlikes
    path('analytics', views.LikeList.as_view()),  # filtering amount of likes by date interval
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),  # getting permission
]