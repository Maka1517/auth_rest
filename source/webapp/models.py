from django.db import models
from django.contrib.auth import get_user_model


# Create your models here.
# Model for storing information about posts
class Post(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Title')
    text = models.TextField(max_length=3000, null=False, blank=False, verbose_name='Text')
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='posts', verbose_name='Author')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation time')
    like_count = models.IntegerField(verbose_name='Like counter', default=0)

    def liked_by(self, user):
        likes = self.likes.filter(user=user)
        return likes.count() > 0

    def __str__(self):
        return "{}. {}".format(self.pk, self.title)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'


# Model for storing information about likes
class PostLike(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                             related_name='post_likes', verbose_name='User')
    post = models.ForeignKey('webapp.Post', on_delete=models.CASCADE,
                                related_name='likes', verbose_name='Post')
    liked_at = models.DateTimeField(auto_now_add=True, verbose_name='Date of like')

    def __str__(self):
        return f'{self.user.name} - {self.post.title}'

    class Meta:
        verbose_name = 'Like'
        verbose_name_plural = 'Post likes'




