from django.contrib import admin
from .models import Post, PostLike


# Register your models here.
# Model for storing information about posts
admin.site.register(Post)
# Model for storing information about likes
admin.site.register(PostLike)


