from django.shortcuts import get_object_or_404
from rest_framework import viewsets, generics
from rest_framework.decorators import action
from rest_framework.response import Response
from webapp.models import Post
from webapp.models import PostLike
from .serializers import PostSerializer, PostLikeSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters import rest_framework as filters


# Post CRUD view by ModelsViewSet DFR
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    # extra function for likes
    @action(methods=['get', 'post'], detail=True)
    def like(self, request, pk=None):
        post = get_object_or_404(Post, pk=pk)
        like, created = PostLike.objects.get_or_create(post=post, user=request.user)
        if created:
            post.like_count += 1
            post.save()
            return Response({'pk': pk, 'likes': post.like_count})
        else:
            return Response(status=403)

    # extra function for unlike
    @action(methods=['get', 'delete'], detail=True)
    def unlike(self, request, pk=None):
        post = get_object_or_404(Post, pk=pk)
        like = get_object_or_404(post.likes, user=request.user)
        like.delete()
        post.like_count -= 1
        post.save()
        return Response({'pk': pk, 'likes': post.like_count})


# class for filtering by date
class PostLikeFilter(filters.FilterSet):
    date_from = filters.DateFilter(field_name="liked_at", lookup_expr='gte')
    date_to = filters.DateFilter(field_name="liked_at", lookup_expr='lte')

    class Meta:
        model = PostLike
        fields = ['date_from', 'date_to']


# likes list view with filtering feature
class LikeList(generics.ListAPIView):
    queryset = PostLike.objects.all()
    serializer_class = PostLikeSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PostLikeFilter
