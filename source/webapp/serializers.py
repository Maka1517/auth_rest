from rest_framework import serializers
from .models import Post, PostLike


# Serializer for posts
class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'title', 'text', 'author', 'created_at', 'like_count']


# Serializer for likes
class PostLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostLike
        fields = ['user', 'post', 'liked_at']