from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
# signals are transferred to models.py
    def ready(self):
        from users import signals


