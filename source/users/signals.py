from logging import error

from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver

from users.models import UserLoginActivity, User


# @receiver(user_logged_in)
# def log_user_logged_in_success(sender, user, request, **kwargs):
#     try:
#         user_agent_info = request.META.get('HTTP_USER_AGENT', '<unknown>')[:255],
#         user_login_activity_log = UserLoginActivity(login_username=user.username,
#                                                     user_agent_info=user_agent_info,
#                                                     status=UserLoginActivity.SUCCESS)
#         user_login_activity_log.save()
#     except Exception as e:
#         # log the error
#         error("log_user_logged_in request: %s, error: %s" % (request, e))
#
#
# @receiver(user_login_failed)
# def log_user_logged_in_failed(sender, credentials, request, **kwargs):
#     try:
#         user_agent_info = request.META.get('HTTP_USER_AGENT', '<unknown>')[:255],
#         user_login_activity_log = UserLoginActivity(login_username=credentials['username'],
#                                                     user_agent_info=user_agent_info,
#                                                     status=UserLoginActivity.FAILED)
#         user_login_activity_log.save()
#     except Exception as e:
#         # log the error
#         error("log_user_logged_in request: %s, error: %s" % (request, e))

# @receiver(user_logged_in)
# def log_user_login(sender, request, user, **kwargs):
#     print('user logged in')
#
#
# @receiver(user_logged_out)
# def log_user_logout(sender, request, user, **kwargs):
#     print('user logged put')