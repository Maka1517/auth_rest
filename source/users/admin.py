from django.contrib import admin
from .models import User, UserLoginActivity


# Register your models here.
# Model for storing information about signed up users
admin.site.register(User)
# Model for storing information about logged in users
admin.site.register(UserLoginActivity)



