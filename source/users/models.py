from logging import error

from django.contrib.auth import user_logged_in, user_login_failed
from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
from django.dispatch import receiver


# Customized User model
class User(AbstractUser):
    name = models.CharField(max_length=250)
    email = models.CharField(max_length=190, unique=True)
    password = models.CharField(max_length=250)

    REQUIRED_FIELDS = ['email', 'password']


# Model for monitoring user activity.
class UserLoginActivity(models.Model):
    # Login Status
    SUCCESS = 'S'
    FAILED = 'F'

    LOGIN_STATUS = ((SUCCESS, 'Success'),
                           (FAILED, 'Failed'))

    login_datetime = models.DateTimeField(auto_now_add=True, verbose_name='Last login')
    login_username = models.CharField(max_length=150, null=True, blank=True)
    status = models.CharField(max_length=1, default=SUCCESS, choices=LOGIN_STATUS, null=True, blank=True)
    user_agent_info = models.CharField(max_length=255)

    def __str__(self):
        return "User name:{}. Last login date {}".format(self.login_username, self.login_datetime)

    class Meta:
        verbose_name = 'user_login_activity'
        verbose_name_plural = 'user_login_activities'



# Signal for recording database when the user is logged in.
# Due to an issue with DFR, signals were transferred to models.py
# otherwise signals are not recognizable
@receiver(user_logged_in)
def log_user_logged_in_success(sender, user, request, **kwargs):
    try:
        user_agent_info = request.META.get('HTTP_USER_AGENT', '<unknown>')[:255],
        user_login_activity_log = UserLoginActivity(login_datetime=user.last_login,
                                                    login_username=user.username,
                                                    user_agent_info=user_agent_info,
                                                    status=UserLoginActivity.SUCCESS,)
        user_login_activity_log.save()
    except Exception as e:
        # log the error
        error("log_user_logged_in request: %s, error: %s" % (request, e))


# @receiver(user_login_failed)
# def log_user_logged_in_failed(sender, credentials, request, **kwargs):
#     try:
#         user_agent_info = request.META.get('HTTP_USER_AGENT', '<unknown>')[:255],
#         user_login_activity_log = UserLoginActivity(login_username=credentials['username'],
#                                                     user_agent_info=user_agent_info,
#                                                     status=UserLoginActivity.FAILED,
#                                                     login_datetime=datetime.now())
#         user_login_activity_log.save()
#     except Exception as e:
#         # log the error
#         error("log_user_logged_in request: %s, error: %s" % (request, e))