from rest_framework import serializers
from .models import User

# Serializer for user model
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password']
        extra_kwargs = {
            'password': {'write_only': True}
        }                                          # for hiding the password in json response

    # function for hashing the password
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data) # ** validated data wout extracted password
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


