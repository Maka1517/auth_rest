from django.urls import path
from .views import RegisterView, LoginView, UserView, LogoutView

urlpatterns = [
    path('register', RegisterView.as_view()),     # for signup
    path('login', LoginView.as_view()),           # for login
    path('user', UserView.as_view()),             # for information about user
    path('logout', LogoutView.as_view()),         # for logout
]